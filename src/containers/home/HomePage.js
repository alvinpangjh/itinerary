import React, { Component } from 'react'
import AddPost from '../../components/addpost/AddPost'
import DisplayPost from '../../components/displaypost/DisplayPost'
import AddPostCard from '../../components/addpost/AddPostCard'
import { config } from '../../components/firebaseConfig/config'
import firebase from 'firebase/app'
import 'firebase/database'

export default class HomePage extends Component {

    //initialize firebase connection
    app = !firebase.apps.length ? firebase.initializeApp(config) : firebase.app()
    database = this.app.database().ref().child('posts')

    state = {
        posts: [],
        showModal: false,
    }

    componentDidMount() {
        const previousPosts = this.state.posts
        this.database.on('child_added', added => {
            previousPosts.push({
                id: added.key, //unique key provided by firebase
                date: added.val().date,
                location: added.val().location,
                details: added.val().details
            })
            this.setState({ posts: previousPosts })
        })
        this.database.on('child_removed', removed =>{
            //loop through all the data in posts and check whether does any of the id equals the key of the post that is to be removed
            //if equal, remove that index with splice
            for(let i=0; i<previousPosts.length; i++){
                if(previousPosts[i].id === removed.key){
                    previousPosts.splice(i,1)
                }
            }
            this.setState({posts: previousPosts})
        })
    }

    addPostHandler = (post) => {
        this.database.push().set({
            date: post.date,
            location: post.location,
            details: post.details
        })
        this.setState({ showModal: false })
    }
    removePostHandler = (id) =>{
        this.database.child(id).remove()
    }

    openPostHandler = () => {
        this.setState({ showModal: !this.state.showModal })
    }

    closePostHandler = () => {
        this.setState({ showModal: false })
    }
    render() {
        return (
            <React.Fragment>
                <AddPost clicked={this.openPostHandler} />
                {this.state.posts.map(post => {
                    return (
                        <DisplayPost
                            key={post.id}
                            id={post.id}
                            date={post.date}
                            location={post.location}
                            details={post.details}
                            removePost={this.removePostHandler} />
                    )
                })}
                {this.state.showModal ? <AddPostCard
                    closed={this.closePostHandler}
                    addPost={this.addPostHandler} /> : null}
            </React.Fragment>
        )
    }
}