import React from 'react'
import './AboutPage.css'

export default function AboutPage() {
    return (
        <div className="about-container">
            <h2>About</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel justo vel risus placerat accumsan efficitur eget velit. Aliquam egestas magna lacus, non porta sapien pulvinar nec. Vestibulum viverra, nisl auctor luctus placerat, mi erat egestas sem, condimentum pretium massa lorem non neque. Nam leo ante, pellentesque quis venenatis in, mollis sit amet justo. Quisque et vestibulum justo. Etiam imperdiet turpis gravida magna sollicitudin tristique nec nec metus. Nullam dui massa, mollis non tortor eget, ornare dignissim est. Nam sagittis rhoncus sapien, sed malesuada urna accumsan volutpat. Vivamus rhoncus semper ipsum, sit amet malesuada justo faucibus sit amet. Aenean nec libero nisl. Sed id est varius, hendrerit eros non, bibendum nibh. Morbi nec luctus libero. Curabitur varius finibus leo, vel blandit est. Fusce lacus est, molestie non lacinia sit amet, interdum at nulla. Nulla lorem ex, convallis id dignissim eu, tempor sed lorem. Sed eu tellus dignissim, viverra velit luctus, fermentum purus. Aenean sit amet tellus et odio gravida auctor. Nullam sagittis, purus in blandit suscipit, enim lorem consequat ipsum, non consequat eros nunc et eros. Curabitur tincidunt, urna non pellentesque porta, leo est tristique nunc, in suscipit mauris sem vitae tellus. Vestibulum finibus neque nulla, vitae congue ipsum convallis id. Maecenas dignissim non ante ut sodales. Donec in molestie lectus. Fusce tempor, tellus vitae facilisis consectetur, velit eros porta massa, a euismod justo purus vitae risus. Vivamus posuere ante metus. Phasellus pulvinar ipsum quis ante tristique mollis. Nunc porttitor sapien vitae mauris malesuada hendrerit. Integer a urna enim. In rhoncus id dolor tristique consectetur. Nullam finibus elit ac dui tincidunt, eu congue lectus vulputate.
            </p>
            <p><strong>Creator :  </strong><span style={{fontStyle:"italic", fontSize: "1.1rem"}}>Alvin Pang</span></p>
        </div>
    )
}