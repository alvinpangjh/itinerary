import React, { Component } from 'react'
import './NavBar.css'
import { FaInfoCircle } from 'react-icons/fa'
import { Link } from 'react-router-dom'

export default class NavBar extends Component {
    state = {
        isClicked: false,
    }

    toggleHandler = () => {
        this.setState({
            isClicked: !this.state.isClicked
        })
    }
    render() {
        return (
            <div className="navigationBar">
                <div className="navigationBar-container">
                    <div className="text-container">
                        <Link to="/">
                            <h1>Itinerary</h1>
                        </Link>
                    </div>
                    <div className="info-container">
                        <Link to='/about' >
                            <FaInfoCircle />
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}