import React, { Component } from 'react'
import './DisplayPost.css'
import { FaWindowClose } from 'react-icons/fa'

export default class DisplayPost extends Component {
    render() {
        return (
            <div className="display-container">
                <div className="date-text">
                    <h4>
                        date: <span>{this.props.date}</span>
                    </h4>
                    <FaWindowClose className="remove-post" onClick={() => this.props.removePost(this.props.id)} />
                </div>
                <div className="location-text">
                    <h4>
                        location: <span>{this.props.location}</span>
                    </h4>
                </div>
                <div className="details-text">
                    <h4>
                        details: <span>{this.props.details}</span>
                    </h4>
                </div>
            </div>
        )
    }
}