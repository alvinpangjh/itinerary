import React from 'react'
import { FaPlusCircle } from 'react-icons/fa'
import './AddPost.css'

export default function (props) {
    return (
        <div className="add-container">
            <div className="add-icon">
                <FaPlusCircle onClick={props.clicked}/>
            </div>
        </div>
    )
}