import React, { Component } from 'react'
import './AddPost.css'
import { FaWindowClose } from 'react-icons/fa'

export default class AddPostCard extends Component {

    state = {
        date: "",
        location: "",
        details: ""
    }

    addHandler = () => {
        this.props.addPost({ ...this.state })
        this.setState({
            date: "",
            location: "",
            details: "",
        })
    }
    clearHandler = () => {
        this.setState({
            date: "",
            location: "",
            details: "",
        })
    }

    render() {
        return (
            <div className="display-card">
                <div className="display-title">add new post</div>
                <FaWindowClose className="close-icon" onClick={this.props.closed} />
                <div className="display-content">
                    <p>date:</p>
                    <input
                        className="input-style"
                        type="text"
                        placeholder="Date of adventure"
                        value={this.state.date}
                        name="date"
                        onChange={(event) => this.setState({ date: event.target.value })} />
                    <br />
                    <p>location:</p>
                    <input
                        className="input-style"
                        type="text"
                        placeholder="Location"
                        value={this.state.location}
                        name="location"
                        onChange={(event) => this.setState({ location: event.target.value })} />
                    <br />
                    <p>things to do</p>
                    <textarea
                        className="input-style2"
                        rows="12"
                        type="text"
                        placeholder="Details"
                        value={this.state.details}
                        name="details"
                        onChange={(event) => this.setState({ details: event.target.value })} />
                    <br />
                    <div className="button-container">
                        <button type="button" className="btn-styles" onClick={this.addHandler}>Add Post</button>
                        <button type="button" className="btn-styles2" onClick={this.clearHandler}>Clear Post</button>
                    </div>
                </div>
            </div>
        )
    }
}