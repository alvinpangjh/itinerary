import React from 'react'
import './App.css'
import { Route } from 'react-router-dom'
import NavBar from './components/navbar/NavBar'
import HomePage from './containers/home/HomePage'
import AboutPage from './components/about/AboutPage'

export default function App() {
  return (
    <React.Fragment>
      <NavBar />
      <Route exact path="/" component={HomePage} />
      <Route exact path="/about" component={AboutPage}/>
    </React.Fragment>
  )
}